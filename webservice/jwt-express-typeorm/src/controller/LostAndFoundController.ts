import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";

import { Hood } from "../entity/Hood";
import { User } from "../entity/User";
import { Lost } from "../entity/Lost";
import { getDistance } from 'geolib';
import { cpus } from "os";
//const distance = 2000; // en metre




class LostAndFoundController {

  static listAll = async (req: Request, res: Response) => {
    //Get hoods from database
    const lostRepository = getRepository(Lost);
    const losts = await lostRepository.find({ relations: ["user"] });
    //Send the hoods objects
    res.send(losts);
  };

  static getOneById = async (req: Request, res: Response) => {
   //Get the ID from the url
   let { id } = req.body;
  
   //Get the user from database
   const lostRepository = getRepository(Lost);
   try {
     const lost = await lostRepository.findOneOrFail(id);
     res.status(200).send(lost);
   } catch (error) {
     res.status(404).send("Lost  not found");
   }
   
  };

  static getOnesByUsername = async (req: Request, res: Response) => {
    //Get the ID from the url
    let { username } = req.body;
    console.log("Username : "+username);
  
    //Get the user from database
    const lostRepository = getRepository(Lost);
    const userRepository = getRepository(User);
    try {
      const user = await  userRepository.findOne({ username }); 
   const hoods = await  lostRepository.find({ user });

      //const user = await userRepository.findOneOrFail(username, {
       //  select: ["username", "id", "role"] //We dont want to send the password on response
      // });
      res.send(hoods);
    } catch (error) {
      res.status(404).send("User not found");
    }
  }; 


  static newLost = async (req: Request, res: Response) => {
    //Get parameters from the body
    let { type, description, name, username, longitude, latitude,date } = req.body;
    let lost = new Lost();
    const lostRepository = getRepository(Lost);
    const userRepository = getRepository(User);
    let date1: Date = new Date(date); 
    console.log('Date from android:',  date);
    console.log('Date AFter Conversion:',  date1);
    const user = await  userRepository.findOne({ username });
    lost.type = type;
    lost.description = description;
    lost.name = name;
    lost.user = user;
    lost.longitude = longitude;
    lost.latitude = latitude;
    lost.date = date;
    //Validade if the parameters are ok
    
let id = 0 ;
    await lostRepository.save(lost).then(lost1 => {
      console.log('Hood  has been saved. Hood  id is:',  id = lost1.id);
      });
    //If all ok, send 201 response
    res.send({
      message: 'Success',
      status: 201,
      id
    });
  };


  static updatePicture = async (identi,picture ) => {
    //Get the ID from the url
    //Get the user from database
    const lostRepository = getRepository(Lost);
      const lost = await  lostRepository.findOne({ id:identi }); 
      lost.picture=picture;
      await  lostRepository.save(lost);   
  
  };


  static editLost = async (req: Request, res: Response) => {
    //Get the ID from the url
    const id = req.params.id;

    //Get values from the body
    const { type, description, name } = req.body;

    //Try to find user on database
    let lost = new Lost();
    const lostRepository = getRepository(Lost);

    try {
        lost = await lostRepository.findOneOrFail(id);
    } catch (error) {
      //If not found, send a 404 response
      res.status(404).send("User not found");
      return;
    }

    //Validate the new values on model
    lost.type = type;
    lost.description = description;
    lost.name = name;
    const errors = await validate(lost);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }

    //Try to safe, if fails, that means username already in use
    try {
      await lostRepository.save(lost);
    } catch (e) {
      res.status(409).send("Hood already in use");
      return;
    }
    //After all send a 204 (no content, but accepted) response
    res.status(204).send();
  };




  static deleteLost = async (req: Request, res: Response) => {
    //Get the ID from the url
    const id = req.params.id;

    const lostRepository = getRepository(Lost);
    let lost: Lost;
    try {
        lost = await lostRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Hood  not found");
      return;
    }
    lostRepository.delete(id);

    //After all send a 204 (no content, but accepted) response
    res.status(204).send();
  };
};

export default LostAndFoundController;