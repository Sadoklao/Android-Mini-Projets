import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";

import { Hood } from "../entity/Hood";
import { User } from "../entity/User";
import { getDistance } from 'geolib';
import { cpus } from "os";
const distance = 1000; // en metre




class HoodController {

  static listAll = async (req: Request, res: Response) => {
    //Get hoods from database
    let { long } = req.body;
    let { latt } = req.body;
    console.log("Longitude From Android : " + long);
    console.log("Lattitude From Android : " + latt);
    const hoodRepository = getRepository(Hood);
    var hoods1 = new Array();
    const hoods = await hoodRepository.find({ relations: ["user"] });
    



    hoods.forEach(function (element) {
      if (getDistance(
        { latitude: long, longitude: latt },
        { latitude: element.latitude.toString(), longitude: element.longitude.toString() }
      ) <= distance) {
  
        hoods1.push(element);
      }
      else {
        console.log("Aucune annonce est proche ");
      }
    });

    res.send(hoods1);
  };

  static getOneById = async (req: Request, res: Response) => {
    //Get the ID from the url
    let { id } = req.body;
  
    //Get the user from database
    const hoodRepository = getRepository(Hood);
    try {
      const hood = await hoodRepository.findOneOrFail(id);
      res.status(200).send(hood);
    } catch (error) {
      res.status(404).send("Hood  not found");
    }
    
  };

  static getOnesByUsername = async (req: Request, res: Response) => {
    //Get the ID from the url
    let { username } = req.body;
    console.log("Username : "+username);
  
    //Get the user from database
    const hoodRepository = getRepository(Hood);
    const userRepository = getRepository(User);
    try {
      const user = await  userRepository.findOne({ username }); 
   const hoods = await  hoodRepository.find({ user });

      //const user = await userRepository.findOneOrFail(username, {
       //  select: ["username", "id", "role"] //We dont want to send the password on response
      // });
      res.send(hoods);
    } catch (error) {
      res.status(404).send("User not found");
    }
  }; 


  static newHood = async (req: Request, res: Response) => {
    //Get parameters from the body
    let { type, content, title, username, longitude, latitude } = req.body;
    let hood = new Hood();
    const hoodRepository = getRepository(Hood);
    const userRepository = getRepository(User);

    const user = await  userRepository.findOne({ username });
    hood.type = type;
    hood.content = content;
    hood.title = title;
    hood.user = user;
    hood.longitude = longitude;
    hood.latitude = latitude;
    //Validade if the parameters are ok
    
let id = 0 ;
    await hoodRepository.save(hood).then(hood1 => {
      console.log('Hood  has been saved. Hood  id is:',  id = hood1.id);
      });
    //If all ok, send 201 response
    res.send({
      message: 'Success',
      status: 201,
      id
    });
  };


  static updatePicture = async (identi,picture ) => {
    //Get the ID from the url
   
  
    //Get the user from database
    const hoodRepository = getRepository(Hood);
  
      const hood = await  hoodRepository.findOne({ id:identi }); 
      hood.picture=picture;
      await  hoodRepository.save(hood);
      //const user = await userRepository.findOneOrFail(username, {
       //  select: ["username", "id", "role"] //We dont want to send the password on response
      // });
   
  
  };
  


  static editHood = async (req: Request, res: Response) => {
    //Get the ID from the url
    const id = req.params.id;

    //Get values from the body
    const { type, content, title } = req.body;

    //Try to find user on database
    let hood = new Hood();
    const hoodRepository = getRepository(Hood);

    try {
      hood = await hoodRepository.findOneOrFail(id);
    } catch (error) {
      //If not found, send a 404 response
      res.status(404).send("User not found");
      return;
    }

    //Validate the new values on model
    hood.type = type;
    hood.content = content;
    hood.title = title;
    const errors = await validate(hood);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }

    //Try to safe, if fails, that means username already in use
    try {
      await hoodRepository.save(hood);
    } catch (e) {
      res.status(409).send("Hood already in use");
      return;
    }
    //After all send a 204 (no content, but accepted) response
    res.status(204).send();
  };

  static deleteHood = async (req: Request, res: Response) => {
    //Get the ID from the url
    const id = req.params.id;

    const hoodRepository = getRepository(Hood);
    let hood: Hood;
    try {
      hood = await hoodRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Hood  not found");
      return;
    }
    hoodRepository.delete(id);

    //After all send a 204 (no content, but accepted) response
    res.status(204).send();
  };
};

export default HoodController;