const connection = require("./Connection");

exports.uploadFile = (request,response)=>{
    let res = {};
    let stat;
    let path = "www.domainname.com/uploads/"+request.file.originalname;
    let type = request.body.name;
    let email = request.body.email;
    let sqlquery;
    if(type == "staff")
    {
        sqlquery = "update organization set StaffDetailsPath = ? where email = ?";
    }
    
    //console.log(type);
    connection.query(sqlquery,[path,email],(error,result)=>{
        if(error)
        {
            res.data = null;
            res.fullerror = error;
            res.message = "cannot able to store the file";
            stat = 500;
        }
        else if(result.affectedRows > 0)
        {
            res.data = null;
            res.fullerror = null;
            res.message = "File uploaded successfully";
            stat = 200;
        }
        else{
            res.data = "null";
            res.fullerror = null;
            res.message = "No Organization available";
            stat = 204;
        }
        writelog("/UploadFile",request.body,res.fullerror,res,"Common");
        response.status(stat).json(res);
    });
    
}