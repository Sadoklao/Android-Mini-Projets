import { Router, Request, Response } from "express";
import auth from "./auth";
import user from "./user";
import hood from "./hood";
import lost from "./lost";

const routes = Router();

routes.use("/auth", auth);
routes.use("/user", user);
routes.use("/hood", hood);
routes.use("/lost", lost);



export default routes;