import { Router } from "express";
import UserController from "../controller/UserController";

import { checkJwt } from "../middlewares/checkJwt";
import { checkRole } from "../middlewares/checkRole";
import * as multer from "multer";

const router = Router();

//Get all users
router.get("/",  UserController.listAll);

// Get one user
/*
router.get(
  "/:id([0-9]+)",
  [checkJwt, checkRole(["ADMIN"])],
  UserController.getOneById
);*/

// Get  user by username
router.post("/me", UserController.getOneByUsername);


//Create a new user
router.post("/", UserController.newUser);

//Create a new user
// router.post("/file",  UserController.newPicture);

//Edit one user
router.post(
  "/edit",
  UserController.editUser
);

//Delete one user
router.delete(
  "/:id([0-9]+)",
  [checkJwt, checkRole(["ADMIN"])],
  UserController.deleteUser
);

export default router;
