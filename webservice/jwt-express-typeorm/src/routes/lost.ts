import { Router } from "express";
import LostAndFoundController from "../controller/LostAndFoundController";
import { checkJwt } from "../middlewares/checkJwt";
import { checkRole } from "../middlewares/checkRole";

const router = Router();

//Get all Lost annonces
router.post("/", LostAndFoundController.listAll);
router.post("/new", LostAndFoundController.newLost);
router.delete("/:id([0-9]+)", LostAndFoundController.deleteLost);
router.post("/me/:id([0-9]+)", LostAndFoundController.getOnesByUsername);
router.post("/get",LostAndFoundController.getOneById)




// Get  Hoods  by user
// router.post("/me", HoodController.getOnesByUsername);


//Create a new user
// router.post("/", [checkJwt, checkRole(["ADMIN"])], HoodController.newHood);

//Edit one Hood
/* router.patch(
  "/:id([0-9]+)",
  [checkJwt, checkRole(["ADMIN"])],
  HoodController.editHood
); */

//Delete one user
/* router.delete(
  "/:id([0-9]+)",
  [checkJwt, checkRole(["ADMIN"])],
  HoodController.deleteHood
); */

export default router;
