package com.espritm.jari.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.espritm.jari.R;
import com.espritm.jari.entites.User;
import com.espritm.jari.ui.TermsFragment;

public class RegisterActivity extends AppCompatActivity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private TextView signIn, terms;
    private ImageView back;
    private TextView email, username, password, retypePassword;
    private Button continu;
    private ProgressDialog pDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Input fileds
        email = findViewById(R.id.register_email);
        username = findViewById(R.id.register_username);
        password = findViewById(R.id.register_password);
        retypePassword = findViewById(R.id.register_retype_password);
        continu = findViewById(R.id.register_continuebtn);
        // Back and signIn buttons
        signIn = findViewById(R.id.signIn);
        back = findViewById(R.id.backBtnAdd);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        terms = findViewById(R.id.termofservice);

        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TermsFragment termsFragment = new TermsFragment();
                termsFragment.show(getSupportFragmentManager(), "TermsFragment");
            }
        });
        // Back button click event
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        // SignIn  button click event
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginPage = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(loginPage);
            }
        });

        // Register Button Click event
        continu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String susername = username.getText().toString().trim();
                String semail = email.getText().toString().trim();
                String spassword = password.getText().toString().trim();
                String sRetypePassword = retypePassword.getText().toString().trim();


                if (!susername.isEmpty() && !semail.isEmpty() && !spassword.isEmpty() && !sRetypePassword.isEmpty()) {
                    if (!spassword.equals(sRetypePassword)) {
                        Toast.makeText(getApplicationContext(),
                                "Password do not match ", Toast.LENGTH_LONG)
                                .show();
                    } else {
                        User user = new User();
                        user.setUsername(susername);
                        user.setEmail(semail);
                        user.setPassword(spassword);

                        Intent i = new Intent(getApplicationContext(),
                                Register2Activity.class);
                        i.putExtra("user", user);
                        startActivity(i);
                        finish();

                    }


                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please complete all fields", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

    }


}