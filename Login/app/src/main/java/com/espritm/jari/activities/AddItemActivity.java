package com.espritm.jari.activities;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;

import com.espritm.jari.R;

import java.util.Calendar;

public class AddItemActivity extends AppCompatActivity {
    Switch Type;
    final Calendar newCalendar = Calendar.getInstance();
    EditText Title, Description, Date;
    ImageView dateicon;
    DatePickerDialog datePickerDialog;
    Button Add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        dateicon = findViewById(R.id.date_icon_add);
        Title = findViewById(R.id.add_title);
        Description = findViewById(R.id.descriptionItem);
        Date = findViewById(R.id.date_Item);
        Type = findViewById(R.id.switch2);
        Add = findViewById(R.id.item_add);
        //Set Date
        dateicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseDate(v);
            }
        });
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int month = monthOfYear + 1;
                Date.setText(year + "/" + month + "/" + dayOfMonth);
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        //Set Switch
        Type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Type.isChecked()) {
                    Type.setTextColor(getResources().getColor(R.color.red));
                    Type.setText("Found");
                    Add.setBackgroundColor(getResources().getColor(R.color.red));
                } else {
                    Type.setTextColor(getResources().getColor(R.color.blue));
                    Type.setText("Lost");
                    Add.setBackgroundColor(getResources().getColor(R.color.blue));
                }
            }
        });


    }
    public void chooseDate(View view) {
        datePickerDialog.show();
    }
}
