package com.espritm.jari.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.espritm.jari.R;
import com.espritm.jari.app.AppController;
import com.espritm.jari.entites.Items;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.annotation.Symbol;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions;
import com.mapbox.mapboxsdk.plugins.markerview.MarkerViewManager;
import com.mapbox.mapboxsdk.utils.ColorUtils;

public class DetailsItemActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String Lost = "marker-15";
    private static final String Found = "marker-15";
    private MapView mapView;
    private PermissionsManager permissionsManager;
    private MapboxMap map;
    private SymbolManager symbolManager;
    private MarkerViewManager markerViewManager;
    private Items i;

    private ImageView image;
    private NetworkImageView picture;
    private TextView username_details,phone_details,description_details,name_details,type;

    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        map = mapboxMap;

        mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style style) {
                symbolManager = new SymbolManager(mapView, mapboxMap, style);
                markerViewManager = new MarkerViewManager(mapView, mapboxMap);
                symbolManager.setIconAllowOverlap(true);
                CameraPosition position = new CameraPosition.Builder()
                        .target(new LatLng(i.getLatitude(), i.getLongitude()))
                        .zoom(15)
                        .tilt(20)
                        .build();
                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 5000);
                if (i.getType() == Items.Type.Found)
                {

                    Symbol s = symbolManager.create(new SymbolOptions()
                            .withLatLng(new LatLng(i.getLatitude(),i.getLongitude()))
                            .withIconImage(Found)
                            .withIconColor(ColorUtils.colorToRgbaString(Color.BLACK))
                            .withTextField(i.getName())
                            .withTextColor(ColorUtils.colorToRgbaString(Color.BLACK))

                            .withIconSize(3f));

                }
                if (i.getType() == Items.Type.Lost)
                {

                    Symbol s = symbolManager.create(new SymbolOptions()
                            .withLatLng(new LatLng(i.getLatitude(),i.getLongitude()))
                            .withIconImage(Lost)
                            .withIconColor(ColorUtils.colorToRgbaString(Color.BLACK))
                            .withTextField(i.getName())

                            .withTextColor(ColorUtils.colorToRgbaString(Color.BLACK))
                            .withIconSize(3f));

                }


            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, "pk.eyJ1Ijoic2Fkb2tsYW8iLCJhIjoiY2szZGppYmUzMTV2djNjbXI5MndweGswMCJ9.T6sd7EhjhccVnVVCCyLuhQ");
        setContentView(R.layout.activity_details);
        mapView = findViewById(R.id.mapView2);
        Intent intent = getIntent();
        Items annonce= (Items) intent.getSerializableExtra("lost");
      //  Toast.makeText(DetailsItemActivity.this, "Identifiant item " + annonce.getTypestring() , Toast.LENGTH_LONG).show();


        username_details = findViewById(R.id.username_details);
        phone_details = findViewById(R.id.phone_details);
        description_details = findViewById(R.id.description_details);
        name_details = findViewById(R.id.name_details);
        type = findViewById(R.id.textView17);
        picture = findViewById(R.id.image_details);


        phone_details.setText(annonce.getNumUser());
        username_details.setText("@"+annonce.getUser());
        description_details.setText(annonce.getDescritption());
        name_details.setText(annonce.getName());
        type.setText(annonce.getTypestring());
        picture.setImageUrl(annonce.getImage(),imageLoader);

        Items.Type type ;
        if (annonce.getTypestring().equals("Lost"))
        {
            type = Items.Type.Lost;
        }
        else if (annonce.getTypestring().equals("Found"))
        {
            type = Items.Type.Found;
        }else{
            type = Items.Type.Found;
        }

        i = new Items(annonce.getLatitude(), annonce.getLongitude(),type,annonce.getName(),annonce.getDescritption());
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

    }
}
