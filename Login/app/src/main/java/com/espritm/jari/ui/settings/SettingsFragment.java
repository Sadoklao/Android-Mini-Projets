package com.espritm.jari.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.espritm.jari.R;
import com.espritm.jari.activities.LoginActivity;
import com.espritm.jari.helper.SessionManager;
import com.espritm.jari.ui.TermsFragment;

public class SettingsFragment extends Fragment {


    Button terms, logout, contact, about;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_settings, container, false);
        terms = root.findViewById(R.id.legalterms);
        logout = root.findViewById(R.id.logout);


        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TermsFragment termsFragment = new TermsFragment();
                termsFragment.show(getFragmentManager(), "TermsFragment");
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionManager session = new SessionManager(getContext());

                session.setLogin(false, "");

                // Launching the login activity
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        return root;
    }


}