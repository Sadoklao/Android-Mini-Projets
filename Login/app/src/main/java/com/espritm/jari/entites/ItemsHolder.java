package com.espritm.jari.entites;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.espritm.jari.R;
import com.espritm.jari.activities.DetailsItemActivity;
import com.espritm.jari.app.AppController;

public class ItemsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    private final NetworkImageView picture;

    private final TextView date;
    private final TextView type;
    private final TextView content;
    private final TextView user;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private Items item;
    private Context context;


    public ItemsHolder(Context context, View itemView) {

        super(itemView);
        // 1. Set the context
        this.context = context;
        // 2. Set up the UI widgets of the holder
        this.picture = itemView.findViewById(R.id.list_lost_img);
        this.date = itemView.findViewById(R.id.list_lost_date);
        this.type = itemView.findViewById(R.id.list_lost_type);
        this.content = itemView.findViewById(R.id.list_lost_content);
        this.user = itemView.findViewById(R.id.list_lost_user);
        // 3. Set the "onClick" listener of the holder
        itemView.setOnClickListener(this);


    }

    public void bindItems(Items item) {

        // 4. Bind the data to the ViewHolder

        this.item = item;
        this.date.setText(item.getDate());
        this.type.setText(item.getTypestring());
        this.content.setText(item.getDescritption());
        this.user.setText("@" + item.getUser());
//        Picasso.with(context).load(item.getPicture()).into(picture);
        this.picture.setImageUrl(item.getImage(), imageLoader);
    }


    @Override
    public void onClick(View v) {

        Intent loginPage = new Intent(context, DetailsItemActivity.class);

        loginPage.putExtra("lost", this.item);
        context.startActivity(loginPage);

    }

}
