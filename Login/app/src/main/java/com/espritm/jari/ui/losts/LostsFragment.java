package com.espritm.jari.ui.losts;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.espritm.jari.R;
import com.espritm.jari.activities.DetailsItemActivity;
import com.espritm.jari.activities.MainScreenActivity;
import com.espritm.jari.entites.Items;
import com.espritm.jari.ui.AddItemFragment;
import com.espritm.jari.ui.Lost2Fragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.Geometry;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.annotation.OnSymbolClickListener;
import com.mapbox.mapboxsdk.plugins.annotation.Symbol;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager;
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions;
import com.mapbox.mapboxsdk.plugins.markerview.MarkerView;
import com.mapbox.mapboxsdk.plugins.markerview.MarkerViewManager;
import com.mapbox.mapboxsdk.plugins.places.picker.PlacePicker;
import com.mapbox.mapboxsdk.plugins.places.picker.model.PlacePickerOptions;
import com.mapbox.mapboxsdk.utils.ColorUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static android.widget.ListPopupWindow.WRAP_CONTENT;
import static com.mapbox.mapboxsdk.Mapbox.getApplicationContext;

public class LostsFragment extends Fragment implements OnMapReadyCallback,
        PermissionsListener {


    private static final String Lost = "marker-15";
    private static final String Found = "marker-stroked-15";
    private static final int PLACE_SELECTION_REQUEST_CODE = 56789;
    private ArrayList<Items> itemsArrayList;
    private ArrayList<Symbol> symbolArrayList;
    private ArrayList<MarkerView> markerArrayList;
    private MapView mapView;
    private MapboxMap map;
    private FloatingActionButton startButton;
    private PermissionsManager permissionsManager;
    private SymbolManager symbolManager;
    private MarkerViewManager markerViewManager;
    private FloatingActionButton add_btn, switcher;
    private TextView selectedLocationTextView;


    private Animation rotation;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        Mapbox.getInstance(inflater.getContext(), "pk.eyJ1Ijoic2Fkb2tsYW8iLCJhIjoiY2szZGppYmUzMTV2djNjbXI5MndweGswMCJ9.T6sd7EhjhccVnVVCCyLuhQ");

        View root = inflater.inflate(R.layout.fragment_losts, container, false);
        selectedLocationTextView = root.findViewById(R.id.locationSelected);
        mapView = root.findViewById(R.id.mapViewLost);
        add_btn = root.findViewById(R.id.add_lost_btn);
        switcher = root.findViewById(R.id.startbtn);


        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToPickerActivity();

            }
        });
        switcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Lost2Fragment losts2Fragment = new Lost2Fragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                fragmentTransaction.replace(R.id.nav_host_fragment, losts2Fragment);
                fragmentTransaction.commit();
            }
        });
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        startButton = root.findViewById(R.id.startbtn);
        MainScreenActivity activity = (MainScreenActivity) getActivity();

        //Random Items
        itemsArrayList = new ArrayList<Items>();
        itemsArrayList = activity.getMyData2();
        Log.e("message", String.valueOf(itemsArrayList.size()));
        /*itemsArrayList.add(new Items(36.9023109, 10.1847953, Items.Type.Lost,"Telephone","Iphone 6 perdu"));
        itemsArrayList.add(new Items(36.9024109, 10.1857953, Items.Type.Lost,"Calculatrice","Iphone 6 perdu"));
        itemsArrayList.add(new Items(36.9025109, 10.1867953, Items.Type.Found,"Telephone","Iphone 6 perdu"));
        itemsArrayList.add(new Items(36.9026109, 10.1877953, Items.Type.Found,"Calculatrice","Iphone 6 perdu"));*/
        symbolArrayList = new ArrayList<>();
        markerArrayList = new ArrayList<>();
        return root;
    }

    private void goToPickerActivity() {
        startActivityForResult(
                new PlacePicker.IntentBuilder()
                        .accessToken("pk.eyJ1Ijoic2Fkb2tsYW8iLCJhIjoiY2szZGppYmUzMTV2djNjbXI5MndweGswMCJ9.T6sd7EhjhccVnVVCCyLuhQ")
                        .placeOptions(PlacePickerOptions.builder()
                                .statingCameraPosition(new CameraPosition.Builder()
                                        .target(new LatLng(36.8081876, 10.1723733)).zoom(16).build())
                                .build())
                        .build(LostsFragment.this.getActivity()), PLACE_SELECTION_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_SELECTION_REQUEST_CODE && resultCode == RESULT_OK) {


            CarmenFeature carmenFeature = PlacePicker.getPlace(data);
            selectedLocationTextView.setText(carmenFeature.text() + " Selected");
            Geometry geometry = carmenFeature.geometry();

            String longlat = "";
            String AddressName = carmenFeature.text();
            try {
                JSONObject point = new JSONObject(geometry.toJson());
                longlat = point.getString("coordinates");
                longlat = longlat.substring(1, longlat.length() - 1);
                String[] strings = longlat.split(",");
                Bundle dataPoint = new Bundle();
                //dataPoint
                dataPoint.putString("Place Name", carmenFeature.text());
                dataPoint.putStringArray("LongLang", strings);
                AddItemFragment AddItem = new AddItemFragment();
                AddItem.setArguments(dataPoint);
                AddItem.show(getFragmentManager(), "AddItemFragment");
                //AddItem.setArguments();


                //Toast.makeText(this.getContext(),strings[0]+" "+strings[1],Toast.LENGTH_SHORT).show();

            } catch (Throwable throwable) {
                Log.e("Jari", "Could not parse malformed JSON: \"" + geometry.toJson() + "\"");
            }

        }
    }


    @Override
    public void onMapReady(@NonNull MapboxMap mapboxMap) {
        map = mapboxMap;

        mapboxMap.setStyle(new Style.Builder().fromUri("mapbox://styles/mapbox/cjerxnqt3cgvp2rmyuxbeqme7"),
                new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        enableLocationComponent(style);

                        symbolManager = new SymbolManager(mapView, mapboxMap, style);
                        markerViewManager = new MarkerViewManager(mapView, mapboxMap);
                        symbolManager.setIconAllowOverlap(true);

                        for (Items i : itemsArrayList) {
                            if (i.getTypestring().equals("Found")) {

                                Symbol s = symbolManager.create(new SymbolOptions()
                                        .withLatLng(new LatLng(i.getLatitude(), i.getLongitude()))
                                        .withIconImage(Found)
                                        .withIconColor(ColorUtils.colorToRgbaString(Color.RED))
                                        .withTextField(i.getName())
                                        .withTextColor(ColorUtils.colorToRgbaString(Color.RED))
                                        .withTextSize(7f)
                                        .withIconSize(1f));
                                symbolArrayList.add(s);
                            }
                            if (i.getTypestring().equals("Lost")) {

                                Symbol s = symbolManager.create(new SymbolOptions()
                                        .withLatLng(new LatLng(i.getLatitude(), i.getLongitude()))
                                        .withIconImage(Lost)
                                        .withIconColor(ColorUtils.colorToRgbaString(Color.GREEN))
                                        .withTextField(i.getName())
                                        .withTextSize(7f)
                                        .withTextColor(ColorUtils.colorToRgbaString(Color.GREEN))
                                        .withIconSize(1f));
                                symbolArrayList.add(s);
                            }
                        }
                        // Add click listener and change the symbol to a cafe icon on click
                        symbolManager.addClickListener(new OnSymbolClickListener() {
                            @Override
                            public void onAnnotationClick(Symbol symbol) {
                                for (MarkerView m : markerArrayList) {
                                    markerViewManager.removeMarker(m);
                                }

                                Items item = itemsArrayList.get(symbolArrayList.indexOf(symbol));
                                // Use an XML layout to create a View object
                                View customView = LayoutInflater.from(LostsFragment.this.getContext()).inflate(
                                        R.layout.marker_view_bubble, null);
                                customView.setLayoutParams(new FrameLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT));

                                // Set the View's TextViews with content
                                TextView titleTextView = customView.findViewById(R.id.marker_window_title);
                                titleTextView.setText(item.getName());

                                TextView snippetTextView = customView.findViewById(R.id.marker_window_snippet);
                                snippetTextView.setText(item.getDescritption());

                                customView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent loginPage = new Intent(getApplicationContext(), DetailsItemActivity.class);

                                        loginPage.putExtra("lost", item);
                                        getApplicationContext().startActivity(loginPage);
                                    }
                                });

                                MarkerView markerView = new MarkerView(new LatLng(item.getLatitude(), item.getLongitude()), customView);
                                markerViewManager.addMarker(markerView);
                                markerArrayList.add(markerView);
                            }

                        });


                    }
                });

    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }


    @SuppressWarnings({"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this.getContext())) {

            // Get an instance of the component
            LocationComponent locationComponent = map.getLocationComponent();
            // Activate with options
            locationComponent.activateLocationComponent(
                    LocationComponentActivationOptions.builder(this.getContext(), loadedMapStyle).build());

            // Enable to make component visible
            locationComponent.setLocationComponentEnabled(true);

            // Set the component's camera mode
            locationComponent.setCameraMode(com.mapbox.mapboxsdk.location.modes.CameraMode.TRACKING_GPS);

            // Set the component's render mode
            locationComponent.setRenderMode(com.mapbox.mapboxsdk.location.modes.RenderMode.NORMAL);
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this.getActivity());
        }
    }


    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this.getContext(), "Permission Required", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            map.getStyle(new Style.OnStyleLoaded() {
                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    enableLocationComponent(style);
                }
            });
        } else {
            Toast.makeText(getContext(), "Permission not Granted", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}



