package com.espritm.jari.entites;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.espritm.jari.R;
import com.espritm.jari.activities.DetailsActivity;
import com.espritm.jari.app.AppController;


public class AnnonceHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final NetworkImageView picture;
    private final TextView date;
    private final TextView type;
    private final TextView content;
    private final TextView user;
    private int id;


    private Annonce annonce;
    private Context context;

    ImageLoader imageLoader = AppController.getInstance().getImageLoader();


    public AnnonceHolder(Context context, View itemView) {

        super(itemView);
        // 1. Set the context
        this.context = context;
        // 2. Set up the UI widgets of the holder
        this.picture = itemView.findViewById(R.id.list_hood_img);
        this.date = itemView.findViewById(R.id.list_hood_date);
        this.type = itemView.findViewById(R.id.list_hood_type);
        this.content = itemView.findViewById(R.id.list_hood_content);
        this.user = itemView.findViewById(R.id.list_hood_user);
        // 3. Set the "onClick" listener of the holder
        itemView.setOnClickListener(this);


    }

    @SuppressLint("SetTextI18n")
    public void bindAnnonce(Annonce annonce) {

        // 4. Bind the data to the ViewHolder

        this.annonce = annonce;
        this.date.setText(annonce.getDate());
        this.type.setText(annonce.getType());
        this.content.setText(annonce.getContent());
        this.user.setText("@" + annonce.getUser());
//       Picasso.with(context).load(annonce.getPicture()).into(picture);
        this.picture.setImageUrl(annonce.getPicture(), imageLoader);
        this.id = annonce.getId();
    }


    @Override
    public void onClick(View v) {

        Intent loginPage = new Intent(context, DetailsActivity.class);

        loginPage.putExtra("hood", annonce);
        context.startActivity(loginPage);

    }


}


