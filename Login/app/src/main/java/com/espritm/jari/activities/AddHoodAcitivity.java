package com.espritm.jari.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.espritm.jari.R;
import com.espritm.jari.app.AppConfig;
import com.espritm.jari.helper.SessionManager;
import com.google.android.gms.location.FusedLocationProviderClient;
import net.gotev.uploadservice.MultipartUploadRequest;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class AddHoodAcitivity extends AppCompatActivity {
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_TAKE_PHOTO = 1;
    ImageView registerimage;
    String currentPhotoPath;
    SessionManager session;
    EditText title, content;
    Spinner type;
    Button addbutton;
    Uri photoURI = null;
    int PERMISSION_ID = 44;
    FusedLocationProviderClient mFusedLocationClient;
    private double longitude = 0;
    private double lattiude = 0;

    public static boolean isNullOrEmpty(String str) {
        return str == null || str.isEmpty();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_hood_acitivity);

        registerimage = findViewById(R.id.add_img);
        title = findViewById(R.id.hood_addTitle);
        content = findViewById(R.id.hood_addcontent);
        type = findViewById(R.id.hood_add_type);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.hood_types, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        type.setAdapter(adapter);
        registerimage = findViewById(R.id.add_img);
        addbutton = findViewById(R.id.hodd_add_button);


        registerimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dispatchTakePictureIntent();

            }
        });
        addbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (haveNetworkConnection()) {
                    if (((isNullOrEmpty(content.getText().toString())) && (isNullOrEmpty(title.getText().toString()))) && (isNullOrEmpty(title.getText().toString()))) {
                        Toast.makeText(getApplicationContext(), "Please Fill all the fields", Toast.LENGTH_SHORT);
                    } else {
                        newHOod(title.getText().toString(), content.getText().toString(), type.getSelectedItem().toString());

                    }

                } else {
                    Toast.makeText(AddHoodAcitivity.this, "Please verify  your connection", Toast.LENGTH_LONG).show();

                }


            }
        });
    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    /**
     * Create a hood and insert into database
     */
    private void newHOod(String stitle, String scontent, String stype) {
        try {

            session = new SessionManager(getApplicationContext());
            SharedPreferences sharedPreferences = getSharedPreferences("location", MODE_PRIVATE);
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("username", session.getUser());
            jsonBody.put("title", stitle);
            jsonBody.put("content", scontent);
            jsonBody.put("type", stype);
            jsonBody.put("latitude", sharedPreferences.getFloat("latt", 0));
            jsonBody.put("longitude", sharedPreferences.getFloat("long", 0));
            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.URL_SET_HOOD_ANNONCES, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.e("VOLLEY", response);
                    try {
                        JSONObject rep = new JSONObject(response);
                        uploadFile(rep.getInt("id"));
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(),
                                "Problem with ID ", Toast.LENGTH_LONG)
                                .show();
                    }

                    // Check the JWT Token
                    Toast.makeText(getApplicationContext(),
                            response, Toast.LENGTH_LONG)
                            .show();
                    finish();


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());

                    Toast.makeText(getApplicationContext(),
                            "A problem has been occured , please retry later.", Toast.LENGTH_LONG)
                            .show();

                }
            }) {
                //                @Override
//                public String getBodyContentType() {
//                    return "application/json; charset=utf-8";
//                }
                //This is for Headers If You Needed
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json; charset=UTF-8");
                    //params.put("auth", LoginToken);
                    return params;
                }


                @Override
                public byte[] getBody() throws AuthFailureError {
                    return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = response.toString();
                        responseString = new String(response.data, StandardCharsets.UTF_8);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void uploadFile(int ident) {
        if (currentPhotoPath == null) {

            Toast.makeText(this, "Please move your  file in the internal storage and retry", Toast.LENGTH_LONG).show();
        } else {
            try {
                String uploadId = UUID.randomUUID().toString();
                //singleUploadBroadcastReceiver.setDelegate(Register2Activity.this);
                //singleUploadBroadcastReceiver.setUploadID(uploadId);

// here I am getting the filename from the path in order to check the file uploading

                String[] fpath = currentPhotoPath.split("/");
                String exactfilename = fpath[fpath.length - 1];
                String[] efilename = exactfilename.split("\\.");
                String ext = efilename[efilename.length - 1];

//Here I am trying to upload the excel file so iam checking whether the file is excel or not

                if (ext.equals("png") || ext.equals("jpg") || ext.equals("jpeg")) {

                    //Creating a multi-part request

                    new MultipartUploadRequest(this, uploadId, AppConfig.URL_UPLOAD_FILE_HOOD)
                            .addFileToUpload(currentPhotoPath, "filedata") //Adding file
                            .addParameter("id", Integer.toString(ident))
                            .addParameter("email", "email")//Adding text parameter to the request
                            .setMaxRetries(2)
                            .startUpload(); //Starting the upload
                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(AddHoodAcitivity.this);
                    builder.setMessage("Please upload a image file").setPositiveButton("ok", null).create().show();

                }

            } catch (Exception exc) {
                Toast.makeText(this, exc.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            //  Bundle extras = data.getExtras();
            //    Bitmap imageBitmap = (Bitmap) extras.get("data");
            registerimage.setImageURI(Uri.parse(currentPhotoPath));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
