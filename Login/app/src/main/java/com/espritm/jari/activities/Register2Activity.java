package com.espritm.jari.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.espritm.jari.R;
import com.espritm.jari.app.AppConfig;
import com.espritm.jari.entites.User;
import com.espritm.jari.helper.FilePath;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions;
import net.gotev.uploadservice.MultipartUploadRequest;
import org.json.JSONException;
import org.json.JSONObject;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class Register2Activity extends AppCompatActivity {

    public static final int REQUEST_CODE_AUTOCOMPLETE = 420;
    private static final String TAG = Register2Activity.class.getSimpleName();
    private static String LoginToken = "";
    final Calendar newCalendar = Calendar.getInstance();
    EditText phone, address, birthdate;
    ImageView dateicon, addressicon;
    DatePickerDialog datePickerDialog;
    Button register;
    PlaceOptions placeOptions = new PlaceOptions() {
        @Nullable
        @Override
        public Point proximity() {
            return null;
        }

        @Nullable
        @Override
        public String language() {
            return null;
        }

        @Override
        public int limit() {
            return 0;
        }

        @Nullable
        @Override
        public Integer historyCount() {
            return null;
        }

        @Nullable
        @Override
        public String bbox() {
            return null;
        }

        @Nullable
        @Override
        public String geocodingTypes() {
            return null;
        }

        @Nullable
        @Override
        public String country() {
            return null;
        }

        @Nullable
        @Override
        public List<String> injectedPlaces() {
            return null;
        }

        @Override
        public int viewMode() {
            return 0;
        }

        @Override
        public int backgroundColor() {
            return 0;
        }

        @Override
        public int toolbarColor() {
            return 0;
        }

        @Override
        public int statusbarColor() {
            return 0;
        }

        @Nullable
        @Override
        public String hint() {
            return null;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {

        }
    };

    //////////////////////////////////////////////////////


    private ImageView registerimage;
    private Uri stafffilePath;


    //////////////////////////////////////////////////////

    public static long getDateDiff(SimpleDateFormat format, String oldDate, String newDate) {
        try {
            return TimeUnit.DAYS.convert(format.parse(newDate).getTime() - format.parse(oldDate).getTime(), TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DateTimeFormatter dateFormatter;
        setContentView(R.layout.activity_register2);
        birthdate = findViewById(R.id.register_birthdate);
        addressicon = findViewById(R.id.addressIcon);
        addressicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new PlaceAutocomplete.IntentBuilder()
                        .accessToken("pk.eyJ1Ijoic2Fkb2tsYW8iLCJhIjoiY2szZGppYmUzMTV2djNjbXI5MndweGswMCJ9.T6sd7EhjhccVnVVCCyLuhQ")
                        .placeOptions(PlaceOptions.builder()
                                .limit(10).build(PlaceOptions.MODE_CARDS))
                        .build(Register2Activity.this);
                startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
            }
        });
        dateicon = findViewById(R.id.date_icon);
        dateicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseDate(v);
            }
        });
        register = findViewById(R.id.item_add);
        phone = findViewById(R.id.register_phone);
        address = findViewById(R.id.register_address);

        registerimage = findViewById(R.id.register_image);
        registerimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseDocument();

            }
        });


        Intent intent = getIntent();
        final User user = (User) intent.getSerializableExtra("user");

        register.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String susername = user.getUsername();
                String semail = user.getEmail();
                String spassword = user.getPassword();
                String sphone = phone.getText().toString().trim();
                String saddress = address.getText().toString().trim();
                String sbirthdate = birthdate.getText().toString().trim();

                if (!sphone.isEmpty() && !saddress.isEmpty() && !spassword.isEmpty() && !sbirthdate.isEmpty()) {
                    registerUser(susername, semail, spassword, sphone, saddress, sbirthdate);

                    uploadFile(stafffilePath, susername);

                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please complete all fields", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });


        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int month = monthOfYear + 1;
                birthdate.setText(year + "/" + month + "/" + dayOfMonth);
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    public void chooseDate(View view) {
        datePickerDialog.show();
    }

    /**
     * function to verify login details in mysql db
     */
    private void registerUser(final String username, final String email, final String password, final String phone, final String address, final String birthdate) {
        try {
            loginWithAdmin();
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("username", username);
            jsonBody.put("password", password);
            jsonBody.put("role", "USER");
            jsonBody.put("address", address);
            jsonBody.put("picture", "USER");
            jsonBody.put("email", email);
            jsonBody.put("phoneNumber", phone);
            jsonBody.put("birthDate", birthdate);
            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.URL_REGISTER, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.e("VOLLEY", response);
                    // Check the JWT Token
                    Toast.makeText(getApplicationContext(),
                            "User created sucessfuly , Try to login now !", Toast.LENGTH_LONG)
                            .show();


                    // Launch MainScreen Acitivty
                    Intent intent = new Intent(
                            Register2Activity.this,
                            LoginActivity.class);
                    startActivity(intent);
                    finish();


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());

                    Toast.makeText(getApplicationContext(),
                            "A problem has been occured , please retry later.", Toast.LENGTH_LONG)
                            .show();

                }
            }) {
                //                @Override
//                public String getBodyContentType() {
//                    return "application/json; charset=utf-8";
//                }
                //This is for Headers If You Needed
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json; charset=UTF-8");
                    params.put("auth", LoginToken);
                    return params;
                }


                @Override
                public byte[] getBody() throws AuthFailureError {
                    return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = response.toString();
                        responseString = new String(response.data, StandardCharsets.UTF_8);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    // Get Token for inscription !

    /**
     * function to verify login details in mysql db
     */
    private void loginWithAdmin() {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("username", "admin");
            jsonBody.put("password", "admin");
            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.URL_LOGIN, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.e("REGISTER TOKEN ", response);
                    // Check the JWT Token
                  /*  Toast.makeText(getApplicationContext(),
                            requestBody, Toast.LENGTH_LONG)
                            .show();*/

                    LoginToken = response;

                    Log.e("LOGIN TOKEN   ", LoginToken);


//                    // Launch MainScreen Acitivty
//                    Intent intent = new Intent(
//                            LoginActivity.this,
//                            MainScreenActivity.class);
//                    startActivity(intent);
//                    finish();


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                    if (error.networkResponse.statusCode == 401) {
                        Toast.makeText(getApplicationContext(),
                                "Please verify your credentials", Toast.LENGTH_LONG)
                                .show();
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "A problem has been occured , please retry later.", Toast.LENGTH_LONG)
                                .show();
                    }
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = response.toString();
//
                        responseString = new String(response.data, StandardCharsets.UTF_8);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void chooseDocument() {
        if (ContextCompat.checkSelfPermission(Register2Activity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            launchIntent();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(Register2Activity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Register2Activity.this);
                builder.setMessage("Allow the permission in order to upload the document")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(Register2Activity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, AppConfig.STORAGE_PERMISSION_CODE);
                            }
                        })
                        .create()
                        .show();

            } else {
                ActivityCompat.requestPermissions(Register2Activity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, AppConfig.STORAGE_PERMISSION_CODE);

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == AppConfig.STORAGE_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(Register2Activity.this, "Permission granted", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void launchIntent() {
        Intent chooseintent = new Intent();

        //If you want to upload an image specify as "image/*"

        chooseintent.setType("image/*");
        chooseintent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(chooseintent, "Select file"), AppConfig.CHOOSE_FILE_STAFF);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConfig.CHOOSE_FILE_STAFF && resultCode == RESULT_OK && data != null && data.getData() != null) {
            stafffilePath = data.getData();
            registerimage.setImageURI(stafffilePath);
           /* Toast.makeText(getApplicationContext(),
                    stafffilePath.toString(), Toast.LENGTH_LONG)
                    .show();*/


        }
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            CarmenFeature feature = PlaceAutocomplete.getPlace(data);
            address = findViewById(R.id.register_address);
            address.setText(feature.text());
            //Toast.makeText(this, feature.text(), Toast.LENGTH_LONG).show();
        }

    }


    public void uploadFile(Uri path, String username) {
        String fullpath = FilePath.getPath(Register2Activity.this, path);


        if (fullpath == null) {

            Toast.makeText(this, "Please move your  file in the internal storage and retry", Toast.LENGTH_LONG).show();
        } else {
            try {
                String uploadId = UUID.randomUUID().toString();
                //singleUploadBroadcastReceiver.setDelegate(Register2Activity.this);
                //singleUploadBroadcastReceiver.setUploadID(uploadId);

// here I am getting the filename from the path in order to check the file uploading

                String[] fpath = fullpath.split("/");
                String exactfilename = fpath[fpath.length - 1];
                String[] efilename = exactfilename.split("\\.");
                String ext = efilename[efilename.length - 1];

//Here I am trying to upload the excel file so iam checking whether the file is excel or not

                if (ext.equals("png") || ext.equals("jpg") || ext.equals("jpeg")) {

                    //Creating a multi-part request

                    new MultipartUploadRequest(this, uploadId, AppConfig.URL_UPLOAD_FILE)
                            .addFileToUpload(fullpath, "filedata") //Adding file
                            .addParameter("name", username)
                            .addParameter("email", "email")//Adding text parameter to the request
                            .setMaxRetries(2)
                            .startUpload(); //Starting the upload
                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(Register2Activity.this);
                    builder.setMessage("Please upload a image file").setPositiveButton("ok", null).create().show();

                }

            } catch (Exception exc) {
                Toast.makeText(this, exc.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }


}


