package com.espritm.jari.ui;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import androidx.core.content.FileProvider;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.espritm.jari.R;
import com.espritm.jari.app.AppConfig;
import com.espritm.jari.entites.Items;
import com.espritm.jari.helper.SessionManager;

import net.gotev.uploadservice.MultipartUploadRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddItemFragment extends DialogFragment {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_TAKE_PHOTO = 1;
    private static final String TAG = "AddItemFragment";
    final Calendar newCalendar = Calendar.getInstance();
    Switch Type;
    EditText Title, Description, Date, Location;
    ImageView dateicon;
    DatePickerDialog datePickerDialog;
    Button Add, Cancel;
    Uri photoURI = null;
    ImageView item_image;
    SessionManager session;
    private String currentPhotoPath;

    public static AddItemFragment newInstance() {
        return new AddItemFragment();
    }

    public static boolean isNullOrEmpty(String str) {
        return str == null || str.isEmpty();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_add_item, container, false);

        dateicon = root.findViewById(R.id.date_icon_add);
        Title = root.findViewById(R.id.add_title);
        Description = root.findViewById(R.id.descriptionItem);
        Date = root.findViewById(R.id.date_Item);
        Type = root.findViewById(R.id.switch2);
        Add = root.findViewById(R.id.item_add);
        Cancel = root.findViewById(R.id.item_cancel);
        Location = root.findViewById(R.id.locationItem);
        item_image = root.findViewById(R.id.item_image);


        //Set Location Name
        Location.setText(getArguments().getString("Place Name"));


        item_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dispatchTakePictureIntent();

            }
        });


        //Set Date
        dateicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseDate(v);
            }
        });
        datePickerDialog = new DatePickerDialog(root.getContext(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int month = monthOfYear + 1;
                Date.setText(year + "/" + month + "/" + dayOfMonth);
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        //Set Switch
        Type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Type.isChecked()) {
                    Type.setTextColor(getResources().getColor(R.color.red));
                    Type.setText("Found");
                    Add.setBackgroundColor(getResources().getColor(R.color.red));
                } else {
                    Type.setTextColor(getResources().getColor(R.color.blue));
                    Type.setText("Lost");
                    Add.setBackgroundColor(getResources().getColor(R.color.blue));
                }
            }
        });

        Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (((isNullOrEmpty(Title.getText().toString())) && (isNullOrEmpty(Description.getText().toString())) && (isNullOrEmpty(Date.getText().toString())))) {

                } else {

                    String[] longlang = getArguments().getStringArray("LongLang");

                    Items item = new Items();
                    item.setName(Title.getText().toString());
                    item.setDescritption(Description.getText().toString());
                    item.setDate(Date.getText().toString());
                    item.setLongitude(Double.parseDouble(longlang[0]));
                    item.setLatitude(Double.parseDouble(longlang[1]));


                    if (Type.isChecked()) {
                        item.setTypestring("Found");
                    } else {
                        item.setTypestring("Lost");
                    }
                    Log.e("LOSTITEM ", item.toString());


                    // getActivity().getFragmentManager().popBackStack();

                    if (Type.isChecked()) {
                        item.setTypestring("Found");
                    } else {
                        item.setTypestring("Lost");
                    }
                    Log.e("LOSTITEM ", item.toString());
                    if (haveNetworkConnection()) {
                        newTag(item);

                    } else {
                        Toast.makeText(getContext(), "Please verify  your connection", Toast.LENGTH_LONG).show();

                    }

                    // getActivity().getFragmentManager().popBackStack();
                    getDialog().dismiss();

                }
            }
        });


        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getDialog().dismiss();

            }
        });

        return root;
    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public void chooseDate(View view) {
        datePickerDialog.show();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(getContext(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            //  Bundle extras = data.getExtras();
            //    Bitmap imageBitmap = (Bitmap) extras.get("data");
            item_image.setImageURI(Uri.parse(currentPhotoPath));
        }
    }

    /**
     * Create a hood and insert into database
     */
    private void newTag(Items item) {
        try {

            session = new SessionManager(getContext());
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("username", session.getUser());
            jsonBody.put("name", item.getName());
            jsonBody.put("description", item.getDescritption());
            jsonBody.put("type", item.getTypestring());
            jsonBody.put("longitude", item.getLongitude());
            jsonBody.put("latitude", item.getLatitude());
            jsonBody.put("date", item.getDate());
            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.URL_SET_LOST, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.e("VOLLEY", response);
                    try {
                        JSONObject rep = new JSONObject(response);
                        uploadFile(rep.getInt("id"));
                    } catch (Exception e) {
                        Toast.makeText(getContext(),
                                "Problem with ID ", Toast.LENGTH_LONG)
                                .show();
                    }

                    // Check the JWT Token
                   /* Toast.makeText(getContext(),
                            response.toString(), Toast.LENGTH_LONG)
                            .show();*/


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());

                    Toast.makeText(getContext(),
                            "A problem has been occured , please retry later.", Toast.LENGTH_LONG)
                            .show();

                }
            }) {
                //                @Override
//                public String getBodyContentType() {
//                    return "application/json; charset=utf-8";
//                }
                //This is for Headers If You Needed
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json; charset=UTF-8");
                    //params.put("auth", LoginToken);
                    return params;
                }


                @Override
                public byte[] getBody() throws AuthFailureError {
                    return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = response.toString();
                        responseString = new String(response.data, StandardCharsets.UTF_8);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void uploadFile(int ident) {
        //String fullpath = FilePath.getPath(Register2Activity.this,path);


        if (currentPhotoPath == null) {

            Toast.makeText(getContext(), "Please move your  file in the internal storage and retry", Toast.LENGTH_LONG).show();
        } else {
            try {
                String uploadId = UUID.randomUUID().toString();
                //singleUploadBroadcastReceiver.setDelegate(Register2Activity.this);
                //singleUploadBroadcastReceiver.setUploadID(uploadId);

// here I am getting the filename from the path in order to check the file uploading

                String[] fpath = currentPhotoPath.split("/");
                String exactfilename = fpath[fpath.length - 1];
                String[] efilename = exactfilename.split("\\.");
                String ext = efilename[efilename.length - 1];

//Here I am trying to upload the excel file so iam checking whether the file is excel or not

                if (ext.equals("png") || ext.equals("jpg") || ext.equals("jpeg")) {

                    //Creating a multi-part request

                    new MultipartUploadRequest(getContext(), uploadId, AppConfig.URL_UPLOAD_FILE_LOST)
                            .addFileToUpload(currentPhotoPath, "filedata") //Adding file
                            .addParameter("id", Integer.toString(ident))
                            .addParameter("email", "email")//Adding text parameter to the request
                            .setMaxRetries(2)
                            .startUpload(); //Starting the upload
                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage("Please upload a image file").setPositiveButton("ok", null).create().show();

                }

            } catch (Exception exc) {
                // Toast.makeText(getContext(), exc.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }


}
