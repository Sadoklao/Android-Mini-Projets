package com.espritm.jari.helper.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.espritm.jari.entites.Items;
import com.espritm.jari.entites.ItemsHolder;

import java.util.List;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsHolder> {


    private final List<Items> items;
    private Context context;
    private int itemResource;


    public ItemsAdapter(Context context, int itemResource, List<Items> items) {

        // 1. Initialize our adapter
        this.items = items;
        this.context = context;
        this.itemResource = itemResource;


    }

    // 2. Override the onCreateViewHolder method
    @Override
    public ItemsHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // 3. Inflate the view and return the new ViewHolder
        View view = LayoutInflater.from(parent.getContext())
                .inflate(this.itemResource, parent, false);
        return new ItemsHolder(this.context, view);
    }

    // 4. Override the onBindViewHolder method
    @Override
    public void onBindViewHolder(ItemsHolder holder, int position) {

        // 5. Use position to access the correct Bakery object
        Items item = this.items.get(position);

        // 6. Bind the bakery object to the holder
        holder.bindItems(item);

    }

    @Override
    public int getItemCount() {

        return this.items.size();
    }
}
