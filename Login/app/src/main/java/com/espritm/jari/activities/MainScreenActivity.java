package com.espritm.jari.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.espritm.jari.R;
import com.espritm.jari.app.AppConfig;
import com.espritm.jari.entites.Annonce;
import com.espritm.jari.entites.Items;
import com.espritm.jari.entites.User;
import com.espritm.jari.helper.SessionManager;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class MainScreenActivity extends AppCompatActivity {
    private static String LoginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpYXQiOjE1NzUyNzYzNzAsImV4cCI6MTU3NTI3OTk3MH0.mLwhj8zDxAtcvOv9qEDMuEXAEIMGJcuqKrpnCF7dV3c";
    ArrayList<Annonce> annonces = new ArrayList<Annonce>();
    ArrayList<Items> losts = new ArrayList<Items>();
    int PERMISSION_ID = 44;
    FusedLocationProviderClient mFusedLocationClient;
    private SessionManager session;
    private double longitude = 0;
    private double lattiude = 0;
    private User user;


    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            //  Toast.makeText(MainScreenActivity.this, "Lattitude : " + mLastLocation.getLatitude() + " Logitude" + mLastLocation.getLongitude(), Toast.LENGTH_LONG).show();


        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = new SessionManager(getApplicationContext());

        Log.e("USERNAME", session.getUser());
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        getLastLocation();
        setContentView(R.layout.activity_main_screen);
        BottomNavigationView navView = findViewById(R.id.nav_view);



        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_hood, R.id.navigation_lostfound, R.id.navigation_profile
                //,R.id.navigation_settings
                , R.id.loadingFragment)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
//        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);


        //mFusedLocationClient.getLastLocation().getResult().getAltitude();
        // Toast.makeText(MainScreenActivity.this, "Lattitude : " + lattiude + " Logitude" + longitude, Toast.LENGTH_LONG).show();


    }

    @SuppressLint("MissingPermission")
    private void getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {

//                                    latTextView.setText(location.getLatitude()+"");
//                                    lonTextView.setText(location.getLongitude()+"");
                                    //Toast.makeText(MainScreenActivity.this, "Lattitude : " + location.getLatitude() + " Logitude" + location.getLongitude(), Toast.LENGTH_LONG).show();


                                    SharedPreferences sharedPreferences = getSharedPreferences("location", MODE_PRIVATE);
                                    final SharedPreferences.Editor editor = sharedPreferences.edit();
// utilisation
                                    editor.putFloat("long", (float) location.getLatitude());
                                    editor.putFloat("latt", (float) location.getLongitude());
                                    editor.commit();

                                    if (haveNetworkConnection()) {
                                        new LoadAllHoods(location.getLatitude(), location.getLongitude()).execute();

                                    } else {
                                        Toast.makeText(MainScreenActivity.this, "Please verify  your connection", Toast.LENGTH_LONG).show();
                                        Intent loginPage = new Intent(MainScreenActivity.this, LoginActivity.class);
                                        startActivity(loginPage);
                                    }
                                    // Toast.makeText(MainScreenActivity.this, "Lattitude : " + lattiude + " Logitude" + longitude, Toast.LENGTH_LONG).show();


                                }
                            }
                        }
                );
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }

    @SuppressLint("MissingPermission")
    private void requestNewLocationData() {

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }


    private boolean checkPermissions() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_ID
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Granted. Start getting the location information
            }
        }
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }


    @Override
    public void onResume() {
        super.onResume();
//        if (checkPermissions()) {
//            getLastLocation();
//        }

    }

    public ArrayList<Annonce> getMyData() {
        return annonces;
    }

    public ArrayList<Items> getMyData2() {
        return losts;
    }

    public User getCurrentUser() {
        return user;
    }

    public boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    /**
     * Get current user details from external database
     */
    private void getCurrentUserDetails(final String username) {
        try {
            //loginWithAdmin();
            RequestQueue requestQueue = Volley.newRequestQueue(MainScreenActivity.this);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("username", username);
            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.URL_CURRENT_USER, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.e("VOLLEY", response);
                    // Check the JWT Token
                       /* Toast.makeText(MainScreenActivity.this,
                                "User details got ", Toast.LENGTH_LONG)
                                .show();*/


                    try {
                        JSONObject jObj = new JSONObject(response);
                        user = new User();
                        // Store  user in RoomDatabase
                        int id = jObj.getInt("id");


                        user.setUsername(jObj.getString("username"));
                        user.setEmail(jObj.getString("email"));
                        user.setPhone(jObj.getString("phoneNumber"));
                        user.setAddress(jObj.getString("address"));
                        String picture = jObj.getString("picture");
                        //user.setCreatedAt(jObj.getString("createdAt"));

                        String imageUri = AppConfig.URL_GET_FILE + picture;
                        user.setPicture(imageUri);
                        // Inserting row in users table
                    } catch (JSONException e) {
                        // JSON error
                        e.printStackTrace();
                        Toast.makeText(MainScreenActivity.this, "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());

                    Toast.makeText(MainScreenActivity.this,
                            "A problem has been occured , please retry later.", Toast.LENGTH_LONG)
                            .show();

                }
            }) {

                //This is for Headers If You Needed
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json; charset=UTF-8");
                    params.put("auth", LoginToken);
                    return params;
                }


                @Override
                public byte[] getBody() throws AuthFailureError {
                    return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = response.toString();
                        responseString = new String(response.data, StandardCharsets.UTF_8);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void loadAllLost() {
        try {

            losts.clear();
            //loginWithAdmin();
            RequestQueue requestQueue = Volley.newRequestQueue(MainScreenActivity.this);
            JSONObject jsonBody = new JSONObject();


            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.URL_GET_LOSTS, new Response.Listener<String>() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void onResponse(String response) {
                    Log.e("VOLLEY", response);
                    try {


                        JSONArray hoods = new JSONArray(response);
                        for (int i = 0; i < hoods.length(); i++) {
                            JSONObject jObj = hoods.getJSONObject(i);

                            int id = jObj.getInt("id");


                            String type = jObj.getString("type");
                            String date = jObj.getString("date");


                            // Instant instant = Instant.parse(date);
                            // Date myDate = Date.from(instant);
                            // SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            // date = formatter.format(myDate);
                            String date1 = getDateDifferenceFromInstant(date);

                            Log.e("DATE", date);
                            String name = jObj.getString("name");
                            String description = jObj.getString("description");
                            String picture = jObj.getString("picture");
                            Double longitude = jObj.getDouble("longitude");
                            Double latitude = jObj.getDouble("latitude");
                            JSONObject user = jObj.getJSONObject("user");
                            String username = user.getString("username");
                            String numero = user.getString("phoneNumber");
                            String imageUri = AppConfig.URL_GET_FILE_LOST + picture;


//
                            Items it = new Items(latitude, longitude, type, name, description, username, date1, imageUri, numero, id);


                            losts.add(it);
                            Log.e("AJOUT REEUSSI", losts.get(0).toString());


                        }


                    } catch (JSONException e) {
                        // JSON error
                        e.printStackTrace();
                        Toast.makeText(MainScreenActivity.this, "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());

                    Toast.makeText(MainScreenActivity.this,
                            "A problem has been occured , please retry later.", Toast.LENGTH_LONG)
                            .show();

                }
            }) {

                //This is for Headers If You Needed
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json; charset=UTF-8");
//                    params.put("auth", LoginToken);
                    return params;
                }


                @Override
                public byte[] getBody() throws AuthFailureError {
                    return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = response.toString();
                        responseString = new String(response.data, StandardCharsets.UTF_8);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            requestQueue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void loadAllHoods2(double longitude, double latitude) {
        try {
            annonces.clear();
            //loginWithAdmin();
            RequestQueue requestQueue = Volley.newRequestQueue(MainScreenActivity.this);
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("long", longitude);
            jsonBody.put("latt", latitude);
            final String requestBody = jsonBody.toString();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.URL_GET_HOOD_ANNONCES, new Response.Listener<String>() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void onResponse(String response) {
                    Log.e("VOLLEY", response);
                    try {


                        JSONArray hoods = new JSONArray(response);
                        for (int i = 0; i < hoods.length(); i++) {
                            JSONObject jObj = hoods.getJSONObject(i);

                            int id = jObj.getInt("id");


                            String type = jObj.getString("type");
                            String date = jObj.getString("date");
                            String date1 = getDateDifferenceFromInstant(date);

//                                Log.e("DATE", date2.toString());
                            String title = jObj.getString("title");
                            String content = jObj.getString("content");
                            String picture = jObj.getString("picture");
                            Double longitude = jObj.getDouble("longitude");
                            Double latitude = jObj.getDouble("latitude");
                            JSONObject user = jObj.getJSONObject("user");

                            String username = user.getString("username");
                            String numero = user.getString("phoneNumber");
                            String imageUri = AppConfig.URL_GET_FILE_HOOD + picture;
//

                            Annonce ann = new Annonce("location", username, content, date1, "00", imageUri, type, id, longitude, latitude);

                            ann.setNumUser(numero);
                            annonces.add(ann);
                            Log.e("AJOUT REEUSSI", annonces.get(0).toString());


                        }


                    } catch (JSONException e) {
                        // JSON error
                        e.printStackTrace();
                        Toast.makeText(MainScreenActivity.this, "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());

                    Toast.makeText(MainScreenActivity.this,
                            "A problem has been occured , please retry later.", Toast.LENGTH_LONG)
                            .show();

                }
            }) {

                //This is for Headers If You Needed
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json; charset=UTF-8");
//                    params.put("auth", LoginToken);
                    return params;
                }


                @Override
                public byte[] getBody() throws AuthFailureError {
                    return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = response.toString();
                        responseString = new String(response.data, StandardCharsets.UTF_8);
                        // can get more details such as response.headers
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };

            requestQueue.add(stringRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public String getDateDifferenceFromInstant(String date) {
        Instant instant = Instant.parse(date);
        Date myDate = Date.from(instant);
        String s = (String) DateUtils.getRelativeDateTimeString(getApplicationContext(), myDate.getTime(), DateUtils.MINUTE_IN_MILLIS, DateUtils.WEEK_IN_MILLIS, 0);
        return s;

    }

    /**
     * Background Async Task to Load all Hoods by making a  HTTP Request
     */
    private class LoadAllHoods extends AsyncTask<Double, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         */

        double longitude, latt;
        AlertDialog dialog;


        LoadAllHoods(double longitude, double latt) {
            this.longitude = longitude;
            this.latt = latt;
            AlertDialog.Builder builder = new AlertDialog.Builder(MainScreenActivity.this);
            builder.setCancelable(false); // if you want user to wait for some process to finish,
            builder.setView(R.layout.layout_loading_dialog);
            dialog = builder.create();
        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            //dialog.show();
            SessionManager session = new SessionManager(MainScreenActivity.this);


        }

        /**
         * getting All Hoods from url
         */
        protected String doInBackground(Double... args) {
            getCurrentUserDetails(session.getUser());
            loadAllLost();
            loadAllHoods2(longitude, latt);
            try {

                annonces.clear();
                //loginWithAdmin();
                RequestQueue requestQueue = Volley.newRequestQueue(MainScreenActivity.this);
                JSONObject jsonBody = new JSONObject();
                jsonBody.put("long", longitude);
                jsonBody.put("latt", latt);

                final String requestBody = jsonBody.toString();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.URL_GET_HOOD_ANNONCES, new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(String response) {
                        Log.e("VOLLEY", response);
                        try {


                            JSONArray hoods = new JSONArray(response);
                            for (int i = 0; i < hoods.length(); i++) {
                                JSONObject jObj = hoods.getJSONObject(i);

                                int id = jObj.getInt("id");


                                String type = jObj.getString("type");
                                String date = jObj.getString("date");
                                String date1 = getDateDifferenceFromInstant(date);

//                                Log.e("DATE", date2.toString());
                                String title = jObj.getString("title");
                                String content = jObj.getString("content");
                                String picture = jObj.getString("picture");
                                Double longitude = jObj.getDouble("longitude");
                                Double latitude = jObj.getDouble("latitude");
                                JSONObject user = jObj.getJSONObject("user");

                                String username = user.getString("username");
                                String numero = user.getString("phoneNumber");
                                String imageUri = AppConfig.URL_GET_FILE_HOOD + picture;
//

                                Annonce ann = new Annonce("location", username, content, date1, "00", imageUri, type, id, longitude, latitude);

                                ann.setNumUser(numero);
                                annonces.add(ann);
                                Log.e("AJOUT REEUSSI", annonces.get(0).toString());


                            }


                        } catch (JSONException e) {
                            // JSON error
                            e.printStackTrace();
                            Toast.makeText(MainScreenActivity.this, "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VOLLEY", error.toString());

                        Toast.makeText(MainScreenActivity.this,
                                "A problem has been occured , please retry later.", Toast.LENGTH_LONG)
                                .show();

                    }
                }) {

                    //This is for Headers If You Needed
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/json; charset=UTF-8");
//                    params.put("auth", LoginToken);
                        return params;
                    }


                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                    }

                    @Override
                    protected Response<String> parseNetworkResponse(NetworkResponse response) {
                        String responseString = "";
                        if (response != null) {
                            responseString = response.toString();
                            responseString = new String(response.data, StandardCharsets.UTF_8);
                            // can get more details such as response.headers
                        }
                        return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                    }
                };

                requestQueue.add(stringRequest);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        protected void onPostExecute(String file_url) {
            // dialog.hide();
        }

    }


}