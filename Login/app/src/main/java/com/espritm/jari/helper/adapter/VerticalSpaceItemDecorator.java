package com.espritm.jari.helper.adapter;


import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

public class VerticalSpaceItemDecorator extends RecyclerView.ItemDecoration {


    private final int verticalSpaceHeight;
    private final int horizentalSpaceHeight;

    public VerticalSpaceItemDecorator(int verticalSpaceHeight,int horizentalSpaceHeight) {

        this.verticalSpaceHeight = verticalSpaceHeight;
        this.horizentalSpaceHeight=horizentalSpaceHeight;
    }

    @Override
    public void getItemOffsets(Rect outRect,
                               View view,
                               RecyclerView parent,
                               RecyclerView.State state) {
        if (parent.getChildAdapterPosition(view)==0)
        {
            outRect.top = verticalSpaceHeight;

        }
        // 1. Determine if we want to add a spacing decorator
        if (parent.getChildAdapterPosition(view) != parent.getAdapter().getItemCount()) {

            // 2. Set the bottom offset to the specified height
            outRect.bottom = verticalSpaceHeight;
            outRect.left=horizentalSpaceHeight;
            outRect.right=horizentalSpaceHeight;

        }
    }
}
