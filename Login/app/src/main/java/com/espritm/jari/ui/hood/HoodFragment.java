package com.espritm.jari.ui.hood;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.espritm.jari.R;
import com.espritm.jari.activities.AddHoodAcitivity;
import com.espritm.jari.activities.MainScreenActivity;
import com.espritm.jari.entites.Annonce;
import com.espritm.jari.helper.adapter.AnnonceAdapter;
import com.espritm.jari.helper.adapter.VerticalSpaceItemDecorator;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;


public class HoodFragment extends Fragment {
    RecyclerView listview;
    ArrayList<Annonce> annonces = new ArrayList<Annonce>();
    Context context;
    AnnonceAdapter annoncesadapter;
    FloatingActionButton fab;
    View root;
    private ProgressDialog pDialog;
    private SwipeRefreshLayout swipeContainer;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        context = container.getContext();

        root = inflater.inflate(R.layout.fragment_hood, container, false);
        listview = root.findViewById(R.id.hood_recycle_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        listview.setLayoutManager(layoutManager);
        //listview.setHasFixedSize(true);
        MainScreenActivity activity = (MainScreenActivity) getActivity();
        annoncesadapter = new AnnonceAdapter(context, R.layout.list_hood_row, activity.getMyData());
        listview.setAdapter(annoncesadapter);
        DefaultItemAnimator itemAnimator = new DefaultItemAnimator();
        VerticalSpaceItemDecorator itemDecorator = new VerticalSpaceItemDecorator((int) getResources().getDimension(R.dimen.spacer_10), (int) getResources().getDimension(R.dimen.spacer_10));
        //ShadowVerticalSpaceItemDecorator shadowItemDecorator = new ShadowVerticalSpaceItemDecorator(this, R.drawable.cell_shadow);
        // 8. Set the ItemDecorators
        //listview.addItemDecoration(shadowItemDecorator);
        listview.addItemDecoration(itemDecorator);
        listview.setItemViewCacheSize(20);
        listview.setDrawingCacheEnabled(true);
        listview.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        // 9. Set the ItemAnimator
        listview.setItemAnimator(itemAnimator);
        annoncesadapter.notifyDataSetChanged();
        fab = root.findViewById(R.id.hood_floatbutton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), AddHoodAcitivity.class));
            }
        });

        swipeContainer = root.findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                fetchTimelineAsync(0);

            }

        });


        return root;
    }

    public void fetchTimelineAsync(int page) {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("location", MODE_PRIVATE);
        if (((MainScreenActivity) getActivity()).haveNetworkConnection()) {
            ((MainScreenActivity) getActivity()).loadAllHoods2(sharedPreferences.getFloat("latt", 0), sharedPreferences.getFloat("long", 0));

        } else {
            Toast.makeText(context, "Please verify  your connection", Toast.LENGTH_LONG).show();

        }
        swipeContainer.setRefreshing(false);
    }


}