package com.espritm.jari.ui.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.espritm.jari.R;
import com.espritm.jari.activities.MainScreenActivity;
import com.espritm.jari.app.AppConfig;
import com.espritm.jari.app.AppController;
import com.espritm.jari.entites.User;
import com.espritm.jari.helper.SessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Date;

import static com.mapbox.mapboxsdk.Mapbox.getApplicationContext;


public class ProfileFragment extends Fragment implements View.OnClickListener {

    private  static final int PICK_IMAGE = 1;
    private static String LoginToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpYXQiOjE1NzUyNzYzNzAsImV4cCI6MTU3NTI3OTk3MH0.mLwhj8zDxAtcvOv9qEDMuEXAEIMGJcuqKrpnCF7dV3c";
    private SessionManager session;

    private TextView email, username1, phone, address, username2,createdAt;
    private TextView profile_edit_address, profile_edit_number, profile_edit_name, profile_edit_phone;
    private Button profile_edit_confirm;
    private Context thiscontext;
    private ImageView image;
    private User user;
    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private boolean changed=false;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        thiscontext = container.getContext();

        View root = inflater.inflate(R.layout.fragment_profile, container, false);

        session = new SessionManager(this.getContext());

        username1 = root.findViewById(R.id.profile_username);
        username2 = root.findViewById(R.id.profile_name);
        email = root.findViewById(R.id.profile_email);
        phone = root.findViewById(R.id.profile_number);
        address = root.findViewById(R.id.profile_address);
        image =  root.findViewById(R.id.profilepic);
        createdAt = root.findViewById(R.id.joinedsince);

        profile_edit_address =  root.findViewById(R.id.profile_edit_address);
        profile_edit_number =  root.findViewById(R.id.profile_edit_number);
        profile_edit_name =  root.findViewById(R.id.profile_edit_name);
        profile_edit_phone =  root.findViewById(R.id.profile_edit_phone);
        profile_edit_confirm =  root.findViewById(R.id.profile_edit_confirm);





        // final TextView textView = root.findViewById(R.id.username);
        TabHost tabs = root.findViewById(R.id.tabhost);
        tabs.setup();
        TabHost.TabSpec spec = tabs.newTabSpec("tag1");
        spec.setContent(R.id.MyProfile);
        spec.setIndicator("My Profile");
        tabs.addTab(spec);
        spec = tabs.newTabSpec("tag2");
        spec.setContent(R.id.EditProfile);
        spec.setIndicator("Edit Profile");
        tabs.addTab(spec);
        MainScreenActivity activity = (MainScreenActivity) getActivity();
        user =activity.getCurrentUser();

        username1.setText(user.getUsername());
        username2.setText(user.getUsername());
        email.setText(user.getEmail());
        phone.setText(user.getPhone());
        address.setText(user.getAddress());
        Picasso.with(getContext()).load(user.getPicture()).into(image);

        //String date1 = getDateDifferenceFromInstant(user.getCreatedAt());
       // createdAt.setText("joined "+date1);



        profile_edit_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               user.setPhone(profile_edit_phone.getText().toString());
                user.setAddress(profile_edit_name.getText().toString());
               user.setUsername(profile_edit_number.getText().toString());
                user.setEmail(profile_edit_address.getText().toString());

                updateUser(user);

            }
        });

        return root;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private String getDateDifferenceFromInstant(String createdAt) {
        Instant instant = Instant.parse(createdAt);
        Date myDate = Date.from(instant);
        String s = (String) DateUtils.getRelativeDateTimeString(getApplicationContext(),myDate.getTime(),DateUtils.MINUTE_IN_MILLIS,DateUtils.WEEK_IN_MILLIS,0);
        return s;
    }


    /**
     * function to verify login details in mysql db
     */
    private void updateUser(User user) {
        try {



                RequestQueue requestQueue = Volley.newRequestQueue(this.getContext());
                JSONObject jsonBody = new JSONObject();
                jsonBody.put("id", user.getId());
                jsonBody.put("username", user.getUsername());
                jsonBody.put("email", user.getEmail());
                jsonBody.put("address", user.getAddress());
                jsonBody.put("id", user.getAddress());
                final String requestBody = jsonBody.toString();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.URL_EDIT_USER, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("EDITUSER ", response);
                        // Check the JWT Token
                       // Toast.makeText(thiscontext, requestBody, Toast.LENGTH_LONG) .show();

                        LoginToken = response;

                        Log.e("EDITUSER", LoginToken);


                        // Launch MainScreen Acitivty
                        Intent intent = new Intent(
                                getContext(),
                                MainScreenActivity.class);
                        startActivity(intent);
                        getActivity().finish();


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VOLLEY", error.toString());
                        if (error.networkResponse.statusCode == 401) {
                            Toast.makeText(thiscontext,
                                    "Please verify your credentials", Toast.LENGTH_LONG)
                                    .show();
                        } else {
                            Toast.makeText(thiscontext,
                                    "A problem has been occured , please retry later.", Toast.LENGTH_LONG)
                                    .show();
                        }
                    }
                }) {
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                    }

                    @Override
                    protected Response<String> parseNetworkResponse(NetworkResponse response) {
                        String responseString = "";
                        if (response != null) {
                            responseString = response.toString();
//
                            responseString = new String(response.data, StandardCharsets.UTF_8);
                            // can get more details such as response.headers
                        }
                        return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                    }
                };

                requestQueue.add(stringRequest);
            } catch(JSONException e){
                e.printStackTrace();
            }

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.profile_edit_upload_image) {
            pickImage();
        }
    }

    public void pickImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE) {
            //TODO: action
        }
    }


}