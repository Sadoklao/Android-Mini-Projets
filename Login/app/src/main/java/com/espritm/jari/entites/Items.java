package com.espritm.jari.entites;

import java.io.Serializable;

public class Items implements Serializable {
    private double latitude, longitude;
    private Type type;
    private String typestring;
    private String name;
    private String descritption;
    private String user;
    private String date;
    private String image;
    private String numUser;
    private int id;
    public Items(double longitude, double altitude, Type type) {
        this.latitude = longitude;
        this.longitude = altitude;
        this.type = type;
    }

    public Items() {

    }

    public Items(double latitude, double longitude, Type type, String name, String descritption) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.type = type;
        this.name = name;
        this.descritption = descritption;
    }


    public Items(double latitude, double longitude, String typestring, String name, String descritption, String user, String date, String image, String numUser, int id) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.typestring = typestring;
        this.name = name;
        this.descritption = descritption;
        this.user = user;
        this.date = date;
        this.image = image;
        this.numUser = numUser;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescritption() {
        return descritption;
    }

    public void setDescritption(String descritption) {
        this.descritption = descritption;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTypestring() {
        return typestring;
    }

    public void setTypestring(String typestring) {
        this.typestring = typestring;
    }

    public String getNumUser() {
        return numUser;
    }

    public void setNumUser(String numUser) {
        this.numUser = numUser;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Items{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", type=" + type +
                ", typestring='" + typestring + '\'' +
                ", name='" + name + '\'' +
                ", descritption='" + descritption + '\'' +
                ", user='" + user + '\'' +
                ", date='" + date + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

    public enum Type {
        Lost, Found
    }
}