package com.espritm.jari.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.espritm.jari.R;

public class WelcomeScreenActivity extends AppCompatActivity {

    TextView signIn;
    Button loginBtn ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);

        loginBtn = findViewById(R.id.register_confirmbtn);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent loginPage = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(loginPage);
                finish();
            }
        });


    }
}
