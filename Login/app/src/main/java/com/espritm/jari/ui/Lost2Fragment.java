package com.espritm.jari.ui;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.espritm.jari.R;
import com.espritm.jari.activities.MainScreenActivity;
import com.espritm.jari.entites.Annonce;
import com.espritm.jari.helper.adapter.ItemsAdapter;
import com.espritm.jari.helper.adapter.VerticalSpaceItemDecorator;
import com.espritm.jari.ui.losts.LostsFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.Geometry;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.plugins.places.picker.PlacePicker;
import com.mapbox.mapboxsdk.plugins.places.picker.model.PlacePickerOptions;

import org.json.JSONObject;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class Lost2Fragment extends Fragment {

    private static final int PLACE_SELECTION_REQUEST_CODE = 56789;
    FloatingActionButton add, switcher;
    RecyclerView listview;
    ArrayList<Annonce> annonces = new ArrayList<Annonce>();
    Context context;
    ItemsAdapter itemsAdapter;
    private SwipeRefreshLayout swipeContainer;
    public Lost2Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = container.getContext();
        View root = inflater.inflate(R.layout.fragment_lost2, container, false);

        listview = root.findViewById(R.id.lost_recycle_view);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);

        listview.setLayoutManager(layoutManager);

        listview.setHasFixedSize(true);

        MainScreenActivity activity = (MainScreenActivity) getActivity();

        itemsAdapter = new ItemsAdapter(context, R.layout.list_items_row, activity.getMyData2());
        listview.setAdapter(itemsAdapter);

        DefaultItemAnimator itemAnimator = new DefaultItemAnimator();

        VerticalSpaceItemDecorator itemDecorator = new VerticalSpaceItemDecorator((int) getResources().getDimension(R.dimen.spacer_10), (int) getResources().getDimension(R.dimen.spacer_10));
        //ShadowVerticalSpaceItemDecorator shadowItemDecorator = new ShadowVerticalSpaceItemDecorator(this, R.drawable.cell_shadow);
        // 8. Set the ItemDecorators
        //listview.addItemDecoration(shadowItemDecorator);
        listview.addItemDecoration(itemDecorator);

        listview.setItemViewCacheSize(20);

        listview.setDrawingCacheEnabled(true);

        listview.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        // 9. Set the ItemAnimator
        listview.setItemAnimator(itemAnimator);


        itemsAdapter.notifyDataSetChanged();

        switcher = root.findViewById(R.id.startbtn2);
        add = root.findViewById(R.id.add_lost_btn2);

        switcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LostsFragment losts2Fragment = new LostsFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.pop_enter, R.anim.pop_exit, R.anim.enter, R.anim.exit);
                fragmentTransaction.replace(R.id.nav_host_fragment, losts2Fragment);
                fragmentTransaction.commit();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToPickerActivity();
            }
        });

        swipeContainer = root.findViewById(R.id.swipeContainer2);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                fetchTimelineAsync(0);

            }

        });


        // Inflate the layout for this fragment
        return root;
    }

    public void fetchTimelineAsync(int page) {

        if (((MainScreenActivity) getActivity()).haveNetworkConnection()) {
            ((MainScreenActivity) getActivity()).loadAllLost();
        } else {
            Toast.makeText(context, "Please verify  your connection", Toast.LENGTH_LONG).show();

        }

        swipeContainer.setRefreshing(false);
    }

    private void goToPickerActivity() {
        startActivityForResult(
                new PlacePicker.IntentBuilder()
                        .accessToken("pk.eyJ1Ijoic2Fkb2tsYW8iLCJhIjoiY2szZGppYmUzMTV2djNjbXI5MndweGswMCJ9.T6sd7EhjhccVnVVCCyLuhQ")
                        .placeOptions(PlacePickerOptions.builder()
                                .statingCameraPosition(new CameraPosition.Builder()
                                        .target(new LatLng(36.8081876, 10.1723733)).zoom(16).build())
                                .build())
                        .build(Lost2Fragment.this.getActivity()), PLACE_SELECTION_REQUEST_CODE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_SELECTION_REQUEST_CODE && resultCode == RESULT_OK) {


            CarmenFeature carmenFeature = PlacePicker.getPlace(data);

            Geometry geometry = carmenFeature.geometry();

            String longlat = "";
            String AddressName = carmenFeature.text();
            try {
                JSONObject point = new JSONObject(geometry.toJson());
                longlat = point.getString("coordinates");
                longlat = longlat.substring(1, longlat.length() - 1);
                String[] strings = longlat.split(",");
                Bundle dataPoint = new Bundle();
                //dataPoint
                dataPoint.putString("Place Name", carmenFeature.text());
                dataPoint.putStringArray("LongLang", strings);
                AddItemFragment AddItem = new AddItemFragment();
                AddItem.setArguments(dataPoint);
                AddItem.show(getFragmentManager(), "AddItemFragment");
                //AddItem.setArguments();


                //Toast.makeText(this.getContext(),strings[0]+" "+strings[1],Toast.LENGTH_SHORT).show();

            } catch (Throwable throwable) {
                Log.e("Jari", "Could not parse malformed JSON: \"" + geometry.toJson() + "\"");
            }

        }
    }
}
