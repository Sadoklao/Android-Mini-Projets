package com.espritm.jari.app;

public class AppConfig {

    // Current address ip
    private static String IP_ADDRESS = "41.226.11.252";

    ////////////////////////// USER /////////////////////////////////////

    // Server user login url
    public static String URL_LOGIN = "http://"+ IP_ADDRESS +":11879/auth/login";
    // Server user register url
    public static String URL_REGISTER = "http://"+ IP_ADDRESS +":11879/user";
    // Server get user  url
    public static String URL_CURRENT_USER = "http://"+ IP_ADDRESS +":11879/user/me";
    // server upload image
    public static String URL_UPLOAD_FILE = "http://"+ IP_ADDRESS +":11879/upload";
    // server get user image
    public static String URL_GET_FILE = "http://"+ IP_ADDRESS +":11879/images/";
    // server EDIT user
    public static String URL_EDIT_USER = "http://"+ IP_ADDRESS +":11879/user/edit";

    ////////////////////////// HOOD /////////////////////////////////////

    // server get hood annonces
    public static String URL_GET_HOOD_ANNONCES = "http://" + IP_ADDRESS + ":11879/hood/";

    // server add new  hood annonces
    public static String URL_SET_HOOD_ANNONCES = "http://" + IP_ADDRESS + ":11879/hood/new";

    // server upload  hood image
    public static String URL_UPLOAD_FILE_HOOD = "http://"+ IP_ADDRESS +":11879/uploadhood";

    // server get hood image
    public static String URL_GET_FILE_HOOD = "http://"+ IP_ADDRESS +":11879/imageshood/";




    ////////////////////////// Lost /////////////////////////////////////
    // server get LOSTS
    public static String URL_GET_LOSTS = "http://" + IP_ADDRESS + ":11879/lost/";

    // server add new  LOST
    public static String URL_SET_LOST = "http://" + IP_ADDRESS + ":11879/lost/new";
    // server upload  hood image
    public static String URL_UPLOAD_FILE_LOST = "http://"+ IP_ADDRESS +":11879/uploadlost";

    // server get hood image
    public static String URL_GET_FILE_LOST = "http://"+ IP_ADDRESS +":11879/imageslost/";





    public static final int STORAGE_PERMISSION_CODE = 123;
    public static final int CHOOSE_FILE_STAFF = 1;
}