package com.espritm.jari;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import androidx.appcompat.app.AppCompatActivity;
import java.util.ArrayList;

public class LostAndFoundActivity extends AppCompatActivity {

    SearchView mySearchView;
    ListView myListView;
    ArrayList<String> list;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lost_and_found);

        mySearchView = findViewById(R.id.mysearchview);
        myListView = findViewById(R.id.mylistview);
        list = new ArrayList<String>();
        list.add("Monday");
        list.add("Tuesday");
        list.add("Mercredi");
        list.add("Jeudi");
        list.add("Saturday");
        list.add("Sunday");

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);

        myListView.setAdapter(adapter);

        mySearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

    }
}
