package com.espritm.jari.helper.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.espritm.jari.entites.Annonce;
import com.espritm.jari.entites.AnnonceHolder;
import java.util.List;


public class AnnonceAdapter extends RecyclerView.Adapter<AnnonceHolder> {


    private final List<Annonce> annonces;
    private Context context;
    private int itemResource;


    public AnnonceAdapter(Context context, int itemResource, List<Annonce> matches) {

        // 1. Initialize our adapter
        this.annonces = matches;
        this.context = context;
        this.itemResource = itemResource;


    }

    // 2. Override the onCreateViewHolder method
    @Override
    public AnnonceHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // 3. Inflate the view and return the new ViewHolder
        View view = LayoutInflater.from(parent.getContext())
                .inflate(this.itemResource, parent, false);
        return new AnnonceHolder(this.context, view);
    }

    // 4. Override the onBindViewHolder method
    @Override
    public void onBindViewHolder(AnnonceHolder holder, int position) {

        // 5. Use position to access the correct Bakery object
        Annonce match = this.annonces.get(position);

        // 6. Bind the bakery object to the holder
        holder.bindAnnonce(match);

    }

    @Override
    public int getItemCount() {

        return this.annonces.size();
    }

    // Clean all elements of the recycler
    public void clear() {
        annonces.clear();
        notifyDataSetChanged();
    }

    // Add a list of items -- change to type used
    public void addAll(List<Annonce> list) {
        annonces.addAll(list);
        notifyDataSetChanged();
    }
}

